<div<?php print $attributes; ?>>
    <div class="block-inner clearfix">
        <?php print render($title_prefix); ?>
        <?php if ($block->subject): ?>
            <div class='sub-menu-config-head'>
                <div class='sub-menu-config-head-title'>
                    <p><?php print $block->subject; ?></p>
                </div>
            </div>

        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <div class='sub-menu-config-content'>
            <div<?php print $content_attributes; ?>>
                <?php print $content ?>
            </div>
        </div>
    </div>
</div>
